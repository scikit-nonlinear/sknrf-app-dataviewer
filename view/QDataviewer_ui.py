# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'QDataviewer.ui'
##
## Created by: Qt User Interface Compiler version 6.4.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QAction, QBrush, QColor, QConicalGradient,
    QCursor, QFont, QFontDatabase, QGradient,
    QIcon, QImage, QKeySequence, QLinearGradient,
    QPainter, QPalette, QPixmap, QRadialGradient,
    QTransform)
from PySide6.QtWidgets import (QApplication, QFrame, QGridLayout, QMainWindow,
    QMenu, QMenuBar, QSizePolicy, QSplitter,
    QStatusBar, QTabWidget, QToolBar, QVBoxLayout,
    QWidget)

from sknrf.app.dataviewer.view.datagroup.datagroup import DatagroupFrame
from sknrf.app.dataviewer.view.equation.equation import EquationFrame
from sknrf.app.dataviewer.view.filter.index import IndexFrame
from sknrf.app.dataviewer.view.image.image import ImageFrame

class Ui_QDataViewer(object):
    def setupUi(self, QDataViewer):
        if not QDataViewer.objectName():
            QDataViewer.setObjectName(u"QDataViewer")
        QDataViewer.resize(1304, 805)
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(QDataViewer.sizePolicy().hasHeightForWidth())
        QDataViewer.setSizePolicy(sizePolicy)
        self.actionExit = QAction(QDataViewer)
        self.actionExit.setObjectName(u"actionExit")
        icon = QIcon(QIcon.fromTheme(u"application-exit"))
        self.actionExit.setIcon(icon)
        self.actionAbout = QAction(QDataViewer)
        self.actionAbout.setObjectName(u"actionAbout")
        icon1 = QIcon(QIcon.fromTheme(u"help-about"))
        self.actionAbout.setIcon(icon1)
        self.actionHelp = QAction(QDataViewer)
        self.actionHelp.setObjectName(u"actionHelp")
        self.actionHelp.setCheckable(True)
        self.actionHelp.setChecked(False)
        icon2 = QIcon()
        iconThemeName = u"help-contents"
        if QIcon.hasThemeIcon(iconThemeName):
            icon2 = QIcon.fromTheme(iconThemeName)
        else:
            icon2.addFile(u".", QSize(), QIcon.Normal, QIcon.Off)

        self.actionHelp.setIcon(icon2)
        self.actionSave = QAction(QDataViewer)
        self.actionSave.setObjectName(u"actionSave")
        icon3 = QIcon()
        iconThemeName = u"document-save"
        if QIcon.hasThemeIcon(iconThemeName):
            icon3 = QIcon.fromTheme(iconThemeName)
        else:
            icon3.addFile(u".", QSize(), QIcon.Normal, QIcon.Off)

        self.actionSave.setIcon(icon3)
        self.actionLoad = QAction(QDataViewer)
        self.actionLoad.setObjectName(u"actionLoad")
        icon4 = QIcon()
        iconThemeName = u"document-open"
        if QIcon.hasThemeIcon(iconThemeName):
            icon4 = QIcon.fromTheme(iconThemeName)
        else:
            icon4.addFile(u".", QSize(), QIcon.Normal, QIcon.Off)

        self.actionLoad.setIcon(icon4)
        self.actionUndo = QAction(QDataViewer)
        self.actionUndo.setObjectName(u"actionUndo")
        icon5 = QIcon()
        iconThemeName = u"edit-undo"
        if QIcon.hasThemeIcon(iconThemeName):
            icon5 = QIcon.fromTheme(iconThemeName)
        else:
            icon5.addFile(u".", QSize(), QIcon.Normal, QIcon.Off)

        self.actionUndo.setIcon(icon5)
        self.actionRedo = QAction(QDataViewer)
        self.actionRedo.setObjectName(u"actionRedo")
        icon6 = QIcon()
        iconThemeName = u"edit-redo"
        if QIcon.hasThemeIcon(iconThemeName):
            icon6 = QIcon.fromTheme(iconThemeName)
        else:
            icon6.addFile(u".", QSize(), QIcon.Normal, QIcon.Off)

        self.actionRedo.setIcon(icon6)
        self.actionExport = QAction(QDataViewer)
        self.actionExport.setObjectName(u"actionExport")
        icon7 = QIcon()
        iconThemeName = u"document-send"
        if QIcon.hasThemeIcon(iconThemeName):
            icon7 = QIcon.fromTheme(iconThemeName)
        else:
            icon7.addFile(u".", QSize(), QIcon.Normal, QIcon.Off)

        self.actionExport.setIcon(icon7)
        self.actionSubplots = QAction(QDataViewer)
        self.actionSubplots.setObjectName(u"actionSubplots")
        icon8 = QIcon(QIcon.fromTheme(u"document-properties"))
        self.actionSubplots.setIcon(icon8)
        self.actionSettings = QAction(QDataViewer)
        self.actionSettings.setObjectName(u"actionSettings")
        icon9 = QIcon()
        iconThemeName = u"applications-system"
        if QIcon.hasThemeIcon(iconThemeName):
            icon9 = QIcon.fromTheme(iconThemeName)
        else:
            icon9.addFile(u".", QSize(), QIcon.Normal, QIcon.Off)

        self.actionSettings.setIcon(icon9)
        self.actionRefresh = QAction(QDataViewer)
        self.actionRefresh.setObjectName(u"actionRefresh")
        icon10 = QIcon()
        iconThemeName = u"view-refresh"
        if QIcon.hasThemeIcon(iconThemeName):
            icon10 = QIcon.fromTheme(iconThemeName)
        else:
            icon10.addFile(u".", QSize(), QIcon.Normal, QIcon.Off)

        self.actionRefresh.setIcon(icon10)
        self.actionClear = QAction(QDataViewer)
        self.actionClear.setObjectName(u"actionClear")
        icon11 = QIcon()
        iconThemeName = u"edit-clear"
        if QIcon.hasThemeIcon(iconThemeName):
            icon11 = QIcon.fromTheme(iconThemeName)
        else:
            icon11.addFile(u".", QSize(), QIcon.Normal, QIcon.Off)

        self.actionClear.setIcon(icon11)
        self.actionNew = QAction(QDataViewer)
        self.actionNew.setObjectName(u"actionNew")
        icon12 = QIcon()
        iconThemeName = u"document-new"
        if QIcon.hasThemeIcon(iconThemeName):
            icon12 = QIcon.fromTheme(iconThemeName)
        else:
            icon12.addFile(u".", QSize(), QIcon.Normal, QIcon.Off)

        self.actionNew.setIcon(icon12)
        self.centralWidget = QWidget(QDataViewer)
        self.centralWidget.setObjectName(u"centralWidget")
        self.verticalLayout = QVBoxLayout(self.centralWidget)
        self.verticalLayout.setSpacing(6)
        self.verticalLayout.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.splitter = QSplitter(self.centralWidget)
        self.splitter.setObjectName(u"splitter")
        self.splitter.setOrientation(Qt.Horizontal)
        self.splitter.setOpaqueResize(True)
        self.splitter.setChildrenCollapsible(True)
        self.sidebarFrame = QFrame(self.splitter)
        self.sidebarFrame.setObjectName(u"sidebarFrame")
        self.sidebarFrame.setFrameShape(QFrame.WinPanel)
        self.sidebarFrame.setFrameShadow(QFrame.Raised)
        self.verticalLayout_2 = QVBoxLayout(self.sidebarFrame)
        self.verticalLayout_2.setSpacing(6)
        self.verticalLayout_2.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.sidebarTabWidget = QTabWidget(self.sidebarFrame)
        self.sidebarTabWidget.setObjectName(u"sidebarTabWidget")
        sizePolicy1 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Expanding)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.sidebarTabWidget.sizePolicy().hasHeightForWidth())
        self.sidebarTabWidget.setSizePolicy(sizePolicy1)
        self.sidebarTabWidget.setTabPosition(QTabWidget.West)
        self.sidebarTabWidget.setElideMode(Qt.ElideNone)
        self.sidebarTabWidget.setUsesScrollButtons(True)
        self.sidebarTabWidget.setDocumentMode(False)
        self.datagroupTab = QWidget()
        self.datagroupTab.setObjectName(u"datagroupTab")
        self.verticalLayout_3 = QVBoxLayout(self.datagroupTab)
        self.verticalLayout_3.setSpacing(6)
        self.verticalLayout_3.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.datagroupFrame = DatagroupFrame(self.datagroupTab)
        self.datagroupFrame.setObjectName(u"datagroupFrame")
        sizePolicy2 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.datagroupFrame.sizePolicy().hasHeightForWidth())
        self.datagroupFrame.setSizePolicy(sizePolicy2)
        self.datagroupFrame.setFrameShape(QFrame.StyledPanel)
        self.datagroupFrame.setFrameShadow(QFrame.Raised)

        self.verticalLayout_3.addWidget(self.datagroupFrame)

        self.sidebarTabWidget.addTab(self.datagroupTab, "")
        self.axesTab = QWidget()
        self.axesTab.setObjectName(u"axesTab")
        self.verticalLayout_4 = QVBoxLayout(self.axesTab)
        self.verticalLayout_4.setSpacing(6)
        self.verticalLayout_4.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.equationFrame = EquationFrame(self.axesTab)
        self.equationFrame.setObjectName(u"equationFrame")
        sizePolicy2.setHeightForWidth(self.equationFrame.sizePolicy().hasHeightForWidth())
        self.equationFrame.setSizePolicy(sizePolicy2)
        self.equationFrame.setFrameShape(QFrame.StyledPanel)
        self.equationFrame.setFrameShadow(QFrame.Raised)

        self.verticalLayout_4.addWidget(self.equationFrame)

        self.sidebarTabWidget.addTab(self.axesTab, "")
        self.indexTab = QWidget()
        self.indexTab.setObjectName(u"indexTab")
        self.verticalLayout_6 = QVBoxLayout(self.indexTab)
        self.verticalLayout_6.setSpacing(6)
        self.verticalLayout_6.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_6.setObjectName(u"verticalLayout_6")
        self.indexFrame = IndexFrame(self.indexTab)
        self.indexFrame.setObjectName(u"indexFrame")
        sizePolicy2.setHeightForWidth(self.indexFrame.sizePolicy().hasHeightForWidth())
        self.indexFrame.setSizePolicy(sizePolicy2)
        self.indexFrame.setFrameShape(QFrame.StyledPanel)
        self.indexFrame.setFrameShadow(QFrame.Raised)

        self.verticalLayout_6.addWidget(self.indexFrame)

        self.sidebarTabWidget.addTab(self.indexTab, "")
        self.imagesTab = QWidget()
        self.imagesTab.setObjectName(u"imagesTab")
        self.verticalLayout_5 = QVBoxLayout(self.imagesTab)
        self.verticalLayout_5.setSpacing(6)
        self.verticalLayout_5.setContentsMargins(11, 11, 11, 11)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.imageFrame = ImageFrame(self.imagesTab)
        self.imageFrame.setObjectName(u"imageFrame")
        sizePolicy2.setHeightForWidth(self.imageFrame.sizePolicy().hasHeightForWidth())
        self.imageFrame.setSizePolicy(sizePolicy2)
        self.imageFrame.setFrameShape(QFrame.StyledPanel)
        self.imageFrame.setFrameShadow(QFrame.Raised)

        self.verticalLayout_5.addWidget(self.imageFrame)

        self.sidebarTabWidget.addTab(self.imagesTab, "")

        self.verticalLayout_2.addWidget(self.sidebarTabWidget)

        self.splitter.addWidget(self.sidebarFrame)
        self.contentFrame = QFrame(self.splitter)
        self.contentFrame.setObjectName(u"contentFrame")
        sizePolicy3 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.contentFrame.sizePolicy().hasHeightForWidth())
        self.contentFrame.setSizePolicy(sizePolicy3)
        self.contentFrame.setFrameShape(QFrame.WinPanel)
        self.contentFrame.setFrameShadow(QFrame.Raised)
        self.gridLayout = QGridLayout(self.contentFrame)
        self.gridLayout.setSpacing(6)
        self.gridLayout.setContentsMargins(11, 11, 11, 11)
        self.gridLayout.setObjectName(u"gridLayout")
        self.contentTabWidget = QTabWidget(self.contentFrame)
        self.contentTabWidget.setObjectName(u"contentTabWidget")
        self.contentTabWidget.setTabPosition(QTabWidget.East)
        self.contentTabWidget.setElideMode(Qt.ElideNone)
        self.contentTabWidget.setUsesScrollButtons(True)
        self.contentTabWidget.setDocumentMode(False)
        self.contentTabWidget.setTabsClosable(True)
        self.contentTabWidget.setMovable(False)

        self.gridLayout.addWidget(self.contentTabWidget, 0, 0, 1, 1)

        self.splitter.addWidget(self.contentFrame)

        self.verticalLayout.addWidget(self.splitter)

        QDataViewer.setCentralWidget(self.centralWidget)
        self.menuBar = QMenuBar(QDataViewer)
        self.menuBar.setObjectName(u"menuBar")
        self.menuBar.setGeometry(QRect(0, 0, 1304, 30))
        self.menuBar.setNativeMenuBar(False)
        self.menuFile = QMenu(self.menuBar)
        self.menuFile.setObjectName(u"menuFile")
        self.menuAbout = QMenu(self.menuBar)
        self.menuAbout.setObjectName(u"menuAbout")
        self.menuEdit = QMenu(self.menuBar)
        self.menuEdit.setObjectName(u"menuEdit")
        QDataViewer.setMenuBar(self.menuBar)
        self.toolBar = QToolBar(QDataViewer)
        self.toolBar.setObjectName(u"toolBar")
        self.toolBar.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        QDataViewer.addToolBar(Qt.TopToolBarArea, self.toolBar)
        self.statusBar = QStatusBar(QDataViewer)
        self.statusBar.setObjectName(u"statusBar")
        QDataViewer.setStatusBar(self.statusBar)

        self.menuBar.addAction(self.menuFile.menuAction())
        self.menuBar.addAction(self.menuEdit.menuAction())
        self.menuBar.addAction(self.menuAbout.menuAction())
        self.menuFile.addAction(self.actionNew)
        self.menuFile.addAction(self.actionLoad)
        self.menuFile.addAction(self.actionSave)
        self.menuFile.addSeparator()
        self.menuFile.addAction(self.actionExport)
        self.menuFile.addAction(self.actionSubplots)
        self.menuFile.addSeparator()
        self.menuFile.addAction(self.actionExit)
        self.menuAbout.addAction(self.actionAbout)
        self.menuAbout.addAction(self.actionHelp)
        self.menuEdit.addAction(self.actionUndo)
        self.menuEdit.addAction(self.actionRedo)
        self.menuEdit.addAction(self.actionRefresh)
        self.menuEdit.addSeparator()
        self.menuEdit.addAction(self.actionClear)
        self.toolBar.addAction(self.actionNew)
        self.toolBar.addAction(self.actionLoad)
        self.toolBar.addAction(self.actionSave)
        self.toolBar.addSeparator()
        self.toolBar.addAction(self.actionSettings)
        self.toolBar.addAction(self.actionHelp)
        self.toolBar.addSeparator()
        self.toolBar.addAction(self.actionExport)
        self.toolBar.addAction(self.actionSubplots)
        self.toolBar.addAction(self.actionUndo)
        self.toolBar.addAction(self.actionRedo)
        self.toolBar.addAction(self.actionRefresh)
        self.toolBar.addAction(self.actionClear)

        self.retranslateUi(QDataViewer)

        self.sidebarTabWidget.setCurrentIndex(0)
        self.contentTabWidget.setCurrentIndex(-1)


        QMetaObject.connectSlotsByName(QDataViewer)
    # setupUi

    def retranslateUi(self, QDataViewer):
        QDataViewer.setWindowTitle(QCoreApplication.translate("QDataViewer", u"DataViewer", None))
        self.actionExit.setText(QCoreApplication.translate("QDataViewer", u"Exit", None))
        self.actionAbout.setText(QCoreApplication.translate("QDataViewer", u"About", None))
        self.actionHelp.setText(QCoreApplication.translate("QDataViewer", u"Help", None))
#if QT_CONFIG(tooltip)
        self.actionHelp.setToolTip(QCoreApplication.translate("QDataViewer", u"Help", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        self.actionHelp.setShortcut(QCoreApplication.translate("QDataViewer", u"Ctrl+/", None))
#endif // QT_CONFIG(shortcut)
        self.actionSave.setText(QCoreApplication.translate("QDataViewer", u"Save", None))
#if QT_CONFIG(tooltip)
        self.actionSave.setToolTip(QCoreApplication.translate("QDataViewer", u"Save Template", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        self.actionSave.setShortcut(QCoreApplication.translate("QDataViewer", u"Ctrl+S", None))
#endif // QT_CONFIG(shortcut)
        self.actionLoad.setText(QCoreApplication.translate("QDataViewer", u"Load", None))
#if QT_CONFIG(tooltip)
        self.actionLoad.setToolTip(QCoreApplication.translate("QDataViewer", u"Load Template", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        self.actionLoad.setShortcut(QCoreApplication.translate("QDataViewer", u"Ctrl+O", None))
#endif // QT_CONFIG(shortcut)
        self.actionUndo.setText(QCoreApplication.translate("QDataViewer", u"Undo", None))
#if QT_CONFIG(shortcut)
        self.actionUndo.setShortcut(QCoreApplication.translate("QDataViewer", u"Ctrl+Z", None))
#endif // QT_CONFIG(shortcut)
        self.actionRedo.setText(QCoreApplication.translate("QDataViewer", u"Redo", None))
#if QT_CONFIG(tooltip)
        self.actionRedo.setToolTip(QCoreApplication.translate("QDataViewer", u"Redo", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        self.actionRedo.setShortcut(QCoreApplication.translate("QDataViewer", u"Ctrl+Shift+Z", None))
#endif // QT_CONFIG(shortcut)
        self.actionExport.setText(QCoreApplication.translate("QDataViewer", u"Export", None))
#if QT_CONFIG(tooltip)
        self.actionExport.setToolTip(QCoreApplication.translate("QDataViewer", u"Export Figure", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        self.actionExport.setShortcut(QCoreApplication.translate("QDataViewer", u"Ctrl+P", None))
#endif // QT_CONFIG(shortcut)
        self.actionSubplots.setText(QCoreApplication.translate("QDataViewer", u"Subplots", None))
#if QT_CONFIG(tooltip)
        self.actionSubplots.setToolTip(QCoreApplication.translate("QDataViewer", u"Configure subplots", None))
#endif // QT_CONFIG(tooltip)
        self.actionSettings.setText(QCoreApplication.translate("QDataViewer", u"Settings", None))
#if QT_CONFIG(tooltip)
        self.actionSettings.setToolTip(QCoreApplication.translate("QDataViewer", u"Settings", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        self.actionSettings.setShortcut(QCoreApplication.translate("QDataViewer", u"Ctrl+,", None))
#endif // QT_CONFIG(shortcut)
        self.actionRefresh.setText(QCoreApplication.translate("QDataViewer", u"Refresh", None))
#if QT_CONFIG(tooltip)
        self.actionRefresh.setToolTip(QCoreApplication.translate("QDataViewer", u"Redraw Figure", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        self.actionRefresh.setShortcut(QCoreApplication.translate("QDataViewer", u"Ctrl+R", None))
#endif // QT_CONFIG(shortcut)
        self.actionClear.setText(QCoreApplication.translate("QDataViewer", u"Clear", None))
#if QT_CONFIG(tooltip)
        self.actionClear.setToolTip(QCoreApplication.translate("QDataViewer", u"Clear Figure", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        self.actionClear.setShortcut(QCoreApplication.translate("QDataViewer", u"Ctrl+C", None))
#endif // QT_CONFIG(shortcut)
        self.actionNew.setText(QCoreApplication.translate("QDataViewer", u"New", None))
#if QT_CONFIG(tooltip)
        self.actionNew.setToolTip(QCoreApplication.translate("QDataViewer", u"New Figure", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(shortcut)
        self.actionNew.setShortcut(QCoreApplication.translate("QDataViewer", u"Ctrl+N", None))
#endif // QT_CONFIG(shortcut)
        self.sidebarTabWidget.setTabText(self.sidebarTabWidget.indexOf(self.datagroupTab), QCoreApplication.translate("QDataViewer", u"Datagroup", None))
        self.sidebarTabWidget.setTabText(self.sidebarTabWidget.indexOf(self.axesTab), QCoreApplication.translate("QDataViewer", u"Axes", None))
        self.sidebarTabWidget.setTabText(self.sidebarTabWidget.indexOf(self.indexTab), QCoreApplication.translate("QDataViewer", u"Index", None))
        self.sidebarTabWidget.setTabText(self.sidebarTabWidget.indexOf(self.imagesTab), QCoreApplication.translate("QDataViewer", u"Images", None))
        self.menuFile.setTitle(QCoreApplication.translate("QDataViewer", u"File", None))
        self.menuAbout.setTitle(QCoreApplication.translate("QDataViewer", u"About", None))
        self.menuEdit.setTitle(QCoreApplication.translate("QDataViewer", u"Edit", None))
    # retranslateUi

