win32: SRC_DIR= $$system(echo %SRC_DIR%)
unix: SRC_DIR= $$system(echo $SRC_DIR)
SRC_DIR=/Users/dylanbespalko/repos/scikit-nonlinear-core-dev
include($$SRC_DIR/sknrf/root.pri)

######## Before Script #########
win32: message($$system(before.bat))
unix: message($$system(sh before.sh))

TARGET = Dataviewer
TEMPLATE = app


SOURCES += \
        main.cpp \
        dataviewer.cpp

HEADERS += \
        dataviewer.h

FORMS += \
    QDatagroupFrame.ui \
    QDataviewer.ui \
    QEquationFrame.ui \
    QImageFrame.ui \
    QIndexFrame.ui \
    QNewFigure.ui

RESOURCES += \
    $$SRC_DIR/sknrf/icons/black.qrc \
    $$SRC_DIR/sknrf/icons/markers.qrc

######## After Script #########
target.path = $$SRC_DIR/sknrf/build
win32: target.extra = after.bat
unix: target.extra = sh after.sh
INSTALLS += target
