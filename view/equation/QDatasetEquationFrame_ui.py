# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'QDatasetEquationFrame.ui'
##
## Created by: Qt User Interface Compiler version 6.4.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QFrame, QHBoxLayout, QLabel,
    QPushButton, QSizePolicy, QSpacerItem, QVBoxLayout,
    QWidget)

from sknrf.view.widget.qtpropertybrowser.base import PropertyScrollArea

class Ui_datasetEquationFrame(object):
    def setupUi(self, datasetEquationFrame):
        if not datasetEquationFrame.objectName():
            datasetEquationFrame.setObjectName(u"datasetEquationFrame")
        datasetEquationFrame.resize(400, 300)
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(datasetEquationFrame.sizePolicy().hasHeightForWidth())
        datasetEquationFrame.setSizePolicy(sizePolicy)
        datasetEquationFrame.setFrameShape(QFrame.WinPanel)
        datasetEquationFrame.setFrameShadow(QFrame.Raised)
        self.verticalLayout = QVBoxLayout(datasetEquationFrame)
        self.verticalLayout.setSpacing(6)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.datasetLabel = QLabel(datasetEquationFrame)
        self.datasetLabel.setObjectName(u"datasetLabel")
        font = QFont()
        font.setBold(True)
        self.datasetLabel.setFont(font)

        self.verticalLayout.addWidget(self.datasetLabel)

        self.propertyTable = PropertyScrollArea(datasetEquationFrame)
        self.propertyTable.setObjectName(u"propertyTable")

        self.verticalLayout.addWidget(self.propertyTable)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.addButton = QPushButton(datasetEquationFrame)
        self.addButton.setObjectName(u"addButton")
        sizePolicy1 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Minimum)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.addButton.sizePolicy().hasHeightForWidth())
        self.addButton.setSizePolicy(sizePolicy1)
        self.addButton.setMinimumSize(QSize(32, 32))
        icon = QIcon(QIcon.fromTheme(u"list-add"))
        self.addButton.setIcon(icon)

        self.horizontalLayout.addWidget(self.addButton)

        self.removeButton = QPushButton(datasetEquationFrame)
        self.removeButton.setObjectName(u"removeButton")
        sizePolicy1.setHeightForWidth(self.removeButton.sizePolicy().hasHeightForWidth())
        self.removeButton.setSizePolicy(sizePolicy1)
        self.removeButton.setMinimumSize(QSize(32, 32))
        icon1 = QIcon(QIcon.fromTheme(u"list-remove"))
        self.removeButton.setIcon(icon1)

        self.horizontalLayout.addWidget(self.removeButton)

        self.infoButton = QPushButton(datasetEquationFrame)
        self.infoButton.setObjectName(u"infoButton")
        sizePolicy1.setHeightForWidth(self.infoButton.sizePolicy().hasHeightForWidth())
        self.infoButton.setSizePolicy(sizePolicy1)
        icon2 = QIcon(QIcon.fromTheme(u"dialog-information"))
        self.infoButton.setIcon(icon2)

        self.horizontalLayout.addWidget(self.infoButton)

        self.upButton = QPushButton(datasetEquationFrame)
        self.upButton.setObjectName(u"upButton")
        sizePolicy1.setHeightForWidth(self.upButton.sizePolicy().hasHeightForWidth())
        self.upButton.setSizePolicy(sizePolicy1)
        self.upButton.setMinimumSize(QSize(32, 32))
        icon3 = QIcon(QIcon.fromTheme(u"go-up"))
        self.upButton.setIcon(icon3)

        self.horizontalLayout.addWidget(self.upButton)

        self.downButton = QPushButton(datasetEquationFrame)
        self.downButton.setObjectName(u"downButton")
        sizePolicy1.setHeightForWidth(self.downButton.sizePolicy().hasHeightForWidth())
        self.downButton.setSizePolicy(sizePolicy1)
        self.downButton.setMinimumSize(QSize(32, 32))
        icon4 = QIcon(QIcon.fromTheme(u"go-down"))
        self.downButton.setIcon(icon4)

        self.horizontalLayout.addWidget(self.downButton)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)


        self.verticalLayout.addLayout(self.horizontalLayout)


        self.retranslateUi(datasetEquationFrame)

        QMetaObject.connectSlotsByName(datasetEquationFrame)
    # setupUi

    def retranslateUi(self, datasetEquationFrame):
        datasetEquationFrame.setWindowTitle(QCoreApplication.translate("datasetEquationFrame", u"Frame", None))
        self.datasetLabel.setText(QCoreApplication.translate("datasetEquationFrame", u"Dataset Equations:", None))
#if QT_CONFIG(tooltip)
        self.propertyTable.setToolTip(QCoreApplication.translate("datasetEquationFrame", u"Property Browser", None))
#endif // QT_CONFIG(tooltip)
#if QT_CONFIG(whatsthis)
        self.propertyTable.setWhatsThis(QCoreApplication.translate("datasetEquationFrame", u"The Property Browser Controls Table Properties", None))
#endif // QT_CONFIG(whatsthis)
        self.addButton.setText("")
        self.removeButton.setText("")
        self.infoButton.setText("")
        self.upButton.setText("")
        self.downButton.setText("")
    # retranslateUi

