# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'QEquationDialog.ui'
##
## Created by: Qt User Interface Compiler version 6.4.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QAbstractButton, QApplication, QDialog, QDialogButtonBox,
    QFrame, QGridLayout, QLabel, QLineEdit,
    QSizePolicy, QVBoxLayout, QWidget)
class Ui_equationDialog(object):
    def setupUi(self, equationDialog):
        if not equationDialog.objectName():
            equationDialog.setObjectName(u"equationDialog")
        equationDialog.resize(362, 159)
        sizePolicy = QSizePolicy(QSizePolicy.MinimumExpanding, QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(equationDialog.sizePolicy().hasHeightForWidth())
        equationDialog.setSizePolicy(sizePolicy)
        equationDialog.setSizeGripEnabled(True)
        equationDialog.setModal(True)
        self.verticalLayout = QVBoxLayout(equationDialog)
        self.verticalLayout.setSpacing(0)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.equationFrame = QFrame(equationDialog)
        self.equationFrame.setObjectName(u"equationFrame")
        self.equationFrame.setFrameShape(QFrame.WinPanel)
        self.equationFrame.setFrameShadow(QFrame.Raised)
        self.gridLayout = QGridLayout(self.equationFrame)
        self.gridLayout.setObjectName(u"gridLayout")
        self.nameLabel = QLabel(self.equationFrame)
        self.nameLabel.setObjectName(u"nameLabel")

        self.gridLayout.addWidget(self.nameLabel, 0, 0, 1, 1)

        self.unitLabel = QLabel(self.equationFrame)
        self.unitLabel.setObjectName(u"unitLabel")

        self.gridLayout.addWidget(self.unitLabel, 2, 0, 1, 1)

        self.unitLineEdit = QLineEdit(self.equationFrame)
        self.unitLineEdit.setObjectName(u"unitLineEdit")

        self.gridLayout.addWidget(self.unitLineEdit, 2, 1, 1, 1)

        self.nameLineEdit = QLineEdit(self.equationFrame)
        self.nameLineEdit.setObjectName(u"nameLineEdit")
        self.nameLineEdit.setEnabled(True)
        self.nameLineEdit.setReadOnly(False)

        self.gridLayout.addWidget(self.nameLineEdit, 0, 1, 1, 1)

        self.equationLineEdit = QLineEdit(self.equationFrame)
        self.equationLineEdit.setObjectName(u"equationLineEdit")

        self.gridLayout.addWidget(self.equationLineEdit, 1, 1, 1, 1)

        self.equationLabel = QLabel(self.equationFrame)
        self.equationLabel.setObjectName(u"equationLabel")

        self.gridLayout.addWidget(self.equationLabel, 1, 0, 1, 1)


        self.verticalLayout.addWidget(self.equationFrame)

        self.buttonBox = QDialogButtonBox(equationDialog)
        self.buttonBox.setObjectName(u"buttonBox")
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)

        self.verticalLayout.addWidget(self.buttonBox)

#if QT_CONFIG(shortcut)
        self.nameLabel.setBuddy(self.nameLineEdit)
        self.unitLabel.setBuddy(self.unitLineEdit)
        self.equationLabel.setBuddy(self.equationLineEdit)
#endif // QT_CONFIG(shortcut)
        QWidget.setTabOrder(self.nameLineEdit, self.equationLineEdit)
        QWidget.setTabOrder(self.equationLineEdit, self.unitLineEdit)
        QWidget.setTabOrder(self.unitLineEdit, self.buttonBox)

        self.retranslateUi(equationDialog)
        self.buttonBox.accepted.connect(equationDialog.accept)
        self.buttonBox.rejected.connect(equationDialog.reject)

        QMetaObject.connectSlotsByName(equationDialog)
    # setupUi

    def retranslateUi(self, equationDialog):
        equationDialog.setWindowTitle(QCoreApplication.translate("equationDialog", u"Dialog", None))
        self.nameLabel.setText(QCoreApplication.translate("equationDialog", u"Name:", None))
        self.unitLabel.setText(QCoreApplication.translate("equationDialog", u"Unit:", None))
        self.nameLineEdit.setText("")
        self.equationLabel.setText(QCoreApplication.translate("equationDialog", u"Equation:", None))
    # retranslateUi

