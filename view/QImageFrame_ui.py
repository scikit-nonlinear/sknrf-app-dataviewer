# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'QImageFrame.ui'
##
## Created by: Qt User Interface Compiler version 6.4.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QAbstractScrollArea, QApplication, QComboBox, QFormLayout,
    QFrame, QHBoxLayout, QLabel, QPlainTextEdit,
    QSizePolicy, QSpacerItem, QToolButton, QVBoxLayout,
    QWidget)

class Ui_imageFrame(object):
    def setupUi(self, imageFrame):
        if not imageFrame.objectName():
            imageFrame.setObjectName(u"imageFrame")
        imageFrame.resize(485, 719)
        imageFrame.setFrameShape(QFrame.StyledPanel)
        imageFrame.setFrameShadow(QFrame.Raised)
        self.formLayout = QFormLayout(imageFrame)
        self.formLayout.setObjectName(u"formLayout")
        self.toolbarFrame = QFrame(imageFrame)
        self.toolbarFrame.setObjectName(u"toolbarFrame")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.toolbarFrame.sizePolicy().hasHeightForWidth())
        self.toolbarFrame.setSizePolicy(sizePolicy)
        self.toolbarFrame.setFrameShape(QFrame.WinPanel)
        self.toolbarFrame.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_2 = QHBoxLayout(self.toolbarFrame)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.exportToolButton = QToolButton(self.toolbarFrame)
        self.exportToolButton.setObjectName(u"exportToolButton")
        icon = QIcon(QIcon.fromTheme(u"document-send"))
        self.exportToolButton.setIcon(icon)
        self.exportToolButton.setIconSize(QSize(24, 24))
        self.exportToolButton.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.exportToolButton.setAutoRaise(True)

        self.horizontalLayout_2.addWidget(self.exportToolButton)

        self.line = QFrame(self.toolbarFrame)
        self.line.setObjectName(u"line")
        self.line.setFrameShape(QFrame.VLine)
        self.line.setFrameShadow(QFrame.Sunken)

        self.horizontalLayout_2.addWidget(self.line)

        self.zoomToolButton = QToolButton(self.toolbarFrame)
        self.zoomToolButton.setObjectName(u"zoomToolButton")
        icon1 = QIcon(QIcon.fromTheme(u"zoom-in"))
        self.zoomToolButton.setIcon(icon1)
        self.zoomToolButton.setIconSize(QSize(24, 24))
        self.zoomToolButton.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.zoomToolButton.setAutoRaise(True)

        self.horizontalLayout_2.addWidget(self.zoomToolButton)

        self.homeToolButton = QToolButton(self.toolbarFrame)
        self.homeToolButton.setObjectName(u"homeToolButton")
        icon2 = QIcon(QIcon.fromTheme(u"zoom-original"))
        self.homeToolButton.setIcon(icon2)
        self.homeToolButton.setIconSize(QSize(24, 24))
        self.homeToolButton.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.homeToolButton.setAutoRaise(True)

        self.horizontalLayout_2.addWidget(self.homeToolButton)

        self.sliceToolButton = QToolButton(self.toolbarFrame)
        self.sliceToolButton.setObjectName(u"sliceToolButton")
        icon3 = QIcon(QIcon.fromTheme(u"x-office-spreadsheet"))
        self.sliceToolButton.setIcon(icon3)
        self.sliceToolButton.setIconSize(QSize(24, 24))
        self.sliceToolButton.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.sliceToolButton.setAutoRaise(True)

        self.horizontalLayout_2.addWidget(self.sliceToolButton)

        self.subplotsToolButton = QToolButton(self.toolbarFrame)
        self.subplotsToolButton.setObjectName(u"subplotsToolButton")
        icon4 = QIcon(QIcon.fromTheme(u"document-properties"))
        self.subplotsToolButton.setIcon(icon4)
        self.subplotsToolButton.setIconSize(QSize(24, 24))
        self.subplotsToolButton.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.subplotsToolButton.setAutoRaise(True)

        self.horizontalLayout_2.addWidget(self.subplotsToolButton)

        self.line_3 = QFrame(self.toolbarFrame)
        self.line_3.setObjectName(u"line_3")
        self.line_3.setFrameShape(QFrame.VLine)
        self.line_3.setFrameShadow(QFrame.Sunken)

        self.horizontalLayout_2.addWidget(self.line_3)

        self.trackingToolButton = QToolButton(self.toolbarFrame)
        self.trackingToolButton.setObjectName(u"trackingToolButton")
        icon5 = QIcon(QIcon.fromTheme(u"media-record"))
        self.trackingToolButton.setIcon(icon5)
        self.trackingToolButton.setIconSize(QSize(24, 24))
        self.trackingToolButton.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.trackingToolButton.setAutoRaise(True)

        self.horizontalLayout_2.addWidget(self.trackingToolButton)

        self.coordinatesComboBox = QComboBox(self.toolbarFrame)
        self.coordinatesComboBox.addItem("")
        self.coordinatesComboBox.addItem("")
        self.coordinatesComboBox.addItem("")
        self.coordinatesComboBox.setObjectName(u"coordinatesComboBox")
        sizePolicy1 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.coordinatesComboBox.sizePolicy().hasHeightForWidth())
        self.coordinatesComboBox.setSizePolicy(sizePolicy1)
        self.coordinatesComboBox.setMinimumSize(QSize(50, 0))

        self.horizontalLayout_2.addWidget(self.coordinatesComboBox)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer)


        self.formLayout.setWidget(0, QFormLayout.SpanningRole, self.toolbarFrame)

        self.videoLabel = QLabel(imageFrame)
        self.videoLabel.setObjectName(u"videoLabel")

        self.formLayout.setWidget(1, QFormLayout.LabelRole, self.videoLabel)

        self.videoComboBox = QComboBox(imageFrame)
        self.videoComboBox.addItem("")
        self.videoComboBox.addItem("")
        self.videoComboBox.addItem("")
        self.videoComboBox.addItem("")
        self.videoComboBox.setObjectName(u"videoComboBox")
        sizePolicy2 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        sizePolicy2.setHorizontalStretch(0)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.videoComboBox.sizePolicy().hasHeightForWidth())
        self.videoComboBox.setSizePolicy(sizePolicy2)

        self.formLayout.setWidget(1, QFormLayout.FieldRole, self.videoComboBox)

        self.colormapLabel = QLabel(imageFrame)
        self.colormapLabel.setObjectName(u"colormapLabel")

        self.formLayout.setWidget(2, QFormLayout.LabelRole, self.colormapLabel)

        self.colormapComboBox = QComboBox(imageFrame)
        self.colormapComboBox.addItem("")
        self.colormapComboBox.addItem("")
        self.colormapComboBox.addItem("")
        self.colormapComboBox.addItem("")
        self.colormapComboBox.addItem("")
        self.colormapComboBox.addItem("")
        self.colormapComboBox.addItem("")
        self.colormapComboBox.addItem("")
        self.colormapComboBox.addItem("")
        self.colormapComboBox.addItem("")
        self.colormapComboBox.setObjectName(u"colormapComboBox")

        self.formLayout.setWidget(2, QFormLayout.FieldRole, self.colormapComboBox)

        self.showLabel = QLabel(imageFrame)
        self.showLabel.setObjectName(u"showLabel")

        self.formLayout.setWidget(3, QFormLayout.LabelRole, self.showLabel)

        self.showComboBox = QComboBox(imageFrame)
        icon6 = QIcon()
        icon6.addFile(u":/icons/PNG/black/32/videocamera.png", QSize(), QIcon.Normal, QIcon.Off)
        self.showComboBox.addItem(icon6, "")
        icon7 = QIcon()
        icon7.addFile(u":/icons/PNG/black/32/stats-dots.png", QSize(), QIcon.Normal, QIcon.Off)
        self.showComboBox.addItem(icon7, "")
        icon8 = QIcon()
        icon8.addFile(u":/PNG/black/32/stats-bars.png", QSize(), QIcon.Normal, QIcon.Off)
        self.showComboBox.addItem(icon8, "")
        icon9 = QIcon()
        icon9.addFile(u":/PNG/black/32/table2.png", QSize(), QIcon.Normal, QIcon.Off)
        self.showComboBox.addItem(icon9, "")
        self.showComboBox.setObjectName(u"showComboBox")

        self.formLayout.setWidget(3, QFormLayout.FieldRole, self.showComboBox)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.formLayout.setItem(4, QFormLayout.SpanningRole, self.verticalSpacer)

        self.exportFrame = QFrame(imageFrame)
        self.exportFrame.setObjectName(u"exportFrame")
        sizePolicy3 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.MinimumExpanding)
        sizePolicy3.setHorizontalStretch(0)
        sizePolicy3.setVerticalStretch(0)
        sizePolicy3.setHeightForWidth(self.exportFrame.sizePolicy().hasHeightForWidth())
        self.exportFrame.setSizePolicy(sizePolicy3)
        self.exportFrame.setFrameShape(QFrame.StyledPanel)
        self.exportFrame.setFrameShadow(QFrame.Raised)
        self.verticalLayout = QVBoxLayout(self.exportFrame)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.contentFigure = QFrame(self.exportFrame)
        self.contentFigure.setObjectName(u"contentFigure")
        sizePolicy3.setHeightForWidth(self.contentFigure.sizePolicy().hasHeightForWidth())
        self.contentFigure.setSizePolicy(sizePolicy3)
        self.contentFigure.setAutoFillBackground(False)
        self.contentFigure.setFrameShape(QFrame.StyledPanel)
        self.contentFigure.setFrameShadow(QFrame.Raised)

        self.verticalLayout.addWidget(self.contentFigure)

        self.coordinatesLabel = QLabel(self.exportFrame)
        self.coordinatesLabel.setObjectName(u"coordinatesLabel")

        self.verticalLayout.addWidget(self.coordinatesLabel)

        self.metadataTextEdit = QPlainTextEdit(self.exportFrame)
        self.metadataTextEdit.setObjectName(u"metadataTextEdit")
        self.metadataTextEdit.setEnabled(True)
        sizePolicy4 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Maximum)
        sizePolicy4.setHorizontalStretch(0)
        sizePolicy4.setVerticalStretch(0)
        sizePolicy4.setHeightForWidth(self.metadataTextEdit.sizePolicy().hasHeightForWidth())
        self.metadataTextEdit.setSizePolicy(sizePolicy4)
        self.metadataTextEdit.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.metadataTextEdit.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.metadataTextEdit.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)
        self.metadataTextEdit.setReadOnly(True)

        self.verticalLayout.addWidget(self.metadataTextEdit)


        self.formLayout.setWidget(6, QFormLayout.SpanningRole, self.exportFrame)

#if QT_CONFIG(shortcut)
        self.videoLabel.setBuddy(self.videoComboBox)
        self.colormapLabel.setBuddy(self.colormapComboBox)
        self.showLabel.setBuddy(self.showComboBox)
#endif // QT_CONFIG(shortcut)

        self.retranslateUi(imageFrame)

        self.videoComboBox.setCurrentIndex(3)
        self.colormapComboBox.setCurrentIndex(2)


        QMetaObject.connectSlotsByName(imageFrame)
    # setupUi

    def retranslateUi(self, imageFrame):
        imageFrame.setWindowTitle(QCoreApplication.translate("imageFrame", u"Frame", None))
        self.exportToolButton.setText(QCoreApplication.translate("imageFrame", u"Export", None))
#if QT_CONFIG(shortcut)
        self.exportToolButton.setShortcut(QCoreApplication.translate("imageFrame", u"Enter", None))
#endif // QT_CONFIG(shortcut)
        self.zoomToolButton.setText(QCoreApplication.translate("imageFrame", u"Zoom", None))
        self.homeToolButton.setText(QCoreApplication.translate("imageFrame", u"Home", None))
        self.sliceToolButton.setText(QCoreApplication.translate("imageFrame", u"Slice", None))
        self.subplotsToolButton.setText(QCoreApplication.translate("imageFrame", u"Subplots", None))
#if QT_CONFIG(tooltip)
        self.trackingToolButton.setToolTip(QCoreApplication.translate("imageFrame", u"Hover/Click Toggle", None))
#endif // QT_CONFIG(tooltip)
        self.trackingToolButton.setText(QCoreApplication.translate("imageFrame", u"Tracking", None))
        self.coordinatesComboBox.setItemText(0, QCoreApplication.translate("imageFrame", u"RI", None))
        self.coordinatesComboBox.setItemText(1, QCoreApplication.translate("imageFrame", u"MA", None))
        self.coordinatesComboBox.setItemText(2, QCoreApplication.translate("imageFrame", u"Z", None))

        self.videoLabel.setText(QCoreApplication.translate("imageFrame", u"Video:", None))
        self.videoComboBox.setItemText(0, QCoreApplication.translate("imageFrame", u"Visible", None))
        self.videoComboBox.setItemText(1, QCoreApplication.translate("imageFrame", u"SWIR", None))
        self.videoComboBox.setItemText(2, QCoreApplication.translate("imageFrame", u"LWIR", None))
        self.videoComboBox.setItemText(3, QCoreApplication.translate("imageFrame", u"MWIR", None))

        self.colormapLabel.setText(QCoreApplication.translate("imageFrame", u"Color Map:", None))
        self.colormapComboBox.setItemText(0, QCoreApplication.translate("imageFrame", u"gray", None))
        self.colormapComboBox.setItemText(1, QCoreApplication.translate("imageFrame", u"gray_log", None))
        self.colormapComboBox.setItemText(2, QCoreApplication.translate("imageFrame", u"qfi", None))
        self.colormapComboBox.setItemText(3, QCoreApplication.translate("imageFrame", u"qfi_log", None))
        self.colormapComboBox.setItemText(4, QCoreApplication.translate("imageFrame", u"plasma", None))
        self.colormapComboBox.setItemText(5, QCoreApplication.translate("imageFrame", u"viridis", None))
        self.colormapComboBox.setItemText(6, QCoreApplication.translate("imageFrame", u"tab10", None))
        self.colormapComboBox.setItemText(7, QCoreApplication.translate("imageFrame", u"tab20", None))
        self.colormapComboBox.setItemText(8, QCoreApplication.translate("imageFrame", u"spectral", None))
        self.colormapComboBox.setItemText(9, QCoreApplication.translate("imageFrame", u"bwr", None))

        self.showLabel.setText(QCoreApplication.translate("imageFrame", u"Show:", None))
        self.showComboBox.setItemText(0, QCoreApplication.translate("imageFrame", u"Image", None))
        self.showComboBox.setItemText(1, QCoreApplication.translate("imageFrame", u"Slice", None))
        self.showComboBox.setItemText(2, QCoreApplication.translate("imageFrame", u"Histogram", None))
        self.showComboBox.setItemText(3, QCoreApplication.translate("imageFrame", u"Correlation Matrix", None))

        self.coordinatesLabel.setText(QCoreApplication.translate("imageFrame", u"<cursor>", None))
        self.metadataTextEdit.setPlainText(QCoreApplication.translate("imageFrame", u"<metadata>", None))
    # retranslateUi

