# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'QNewFigure.ui'
##
## Created by: Qt User Interface Compiler version 6.4.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QAbstractButton, QApplication, QDialog, QDialogButtonBox,
    QFormLayout, QGridLayout, QGroupBox, QLabel,
    QLineEdit, QSizePolicy, QSpacerItem, QSpinBox,
    QWidget)

class Ui_QNewFigureDialog(object):
    def setupUi(self, QNewFigureDialog):
        if not QNewFigureDialog.objectName():
            QNewFigureDialog.setObjectName(u"QNewFigureDialog")
        QNewFigureDialog.resize(400, 155)
        self.formLayout = QFormLayout(QNewFigureDialog)
        self.formLayout.setObjectName(u"formLayout")
        self.nameLabel = QLabel(QNewFigureDialog)
        self.nameLabel.setObjectName(u"nameLabel")

        self.formLayout.setWidget(0, QFormLayout.LabelRole, self.nameLabel)

        self.nameLineEdit = QLineEdit(QNewFigureDialog)
        self.nameLineEdit.setObjectName(u"nameLineEdit")

        self.formLayout.setWidget(0, QFormLayout.FieldRole, self.nameLineEdit)

        self.gridGroupBox = QGroupBox(QNewFigureDialog)
        self.gridGroupBox.setObjectName(u"gridGroupBox")
        self.gridLayout = QGridLayout(self.gridGroupBox)
        self.gridLayout.setObjectName(u"gridLayout")
        self.widthLabel = QLabel(self.gridGroupBox)
        self.widthLabel.setObjectName(u"widthLabel")

        self.gridLayout.addWidget(self.widthLabel, 0, 0, 1, 1)

        self.widthGridSpinBox = QSpinBox(self.gridGroupBox)
        self.widthGridSpinBox.setObjectName(u"widthGridSpinBox")
        self.widthGridSpinBox.setMinimum(1)

        self.gridLayout.addWidget(self.widthGridSpinBox, 0, 1, 1, 1)

        self.heightGridLabel = QLabel(self.gridGroupBox)
        self.heightGridLabel.setObjectName(u"heightGridLabel")

        self.gridLayout.addWidget(self.heightGridLabel, 0, 2, 1, 1)

        self.heightGridSpinBox = QSpinBox(self.gridGroupBox)
        self.heightGridSpinBox.setObjectName(u"heightGridSpinBox")
        self.heightGridSpinBox.setMinimum(1)

        self.gridLayout.addWidget(self.heightGridSpinBox, 0, 3, 1, 1)


        self.formLayout.setWidget(1, QFormLayout.SpanningRole, self.gridGroupBox)

        self.buttonBox = QDialogButtonBox(QNewFigureDialog)
        self.buttonBox.setObjectName(u"buttonBox")
        self.buttonBox.setOrientation(Qt.Horizontal)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)

        self.formLayout.setWidget(3, QFormLayout.SpanningRole, self.buttonBox)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.formLayout.setItem(2, QFormLayout.LabelRole, self.verticalSpacer)


        self.retranslateUi(QNewFigureDialog)
        self.buttonBox.accepted.connect(QNewFigureDialog.accept)
        self.buttonBox.rejected.connect(QNewFigureDialog.reject)

        QMetaObject.connectSlotsByName(QNewFigureDialog)
    # setupUi

    def retranslateUi(self, QNewFigureDialog):
        QNewFigureDialog.setWindowTitle(QCoreApplication.translate("QNewFigureDialog", u"Dialog", None))
        self.nameLabel.setText(QCoreApplication.translate("QNewFigureDialog", u"Name:", None))
        self.gridGroupBox.setTitle(QCoreApplication.translate("QNewFigureDialog", u"Grid", None))
        self.widthLabel.setText(QCoreApplication.translate("QNewFigureDialog", u"Width:", None))
        self.heightGridLabel.setText(QCoreApplication.translate("QNewFigureDialog", u"Height:", None))
    # retranslateUi

