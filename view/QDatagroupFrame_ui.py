# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'QDatagroupFrame.ui'
##
## Created by: Qt User Interface Compiler version 6.4.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QAbstractItemView, QApplication, QFrame, QGridLayout,
    QGroupBox, QHBoxLayout, QHeaderView, QLineEdit,
    QPushButton, QSizePolicy, QSpacerItem, QToolButton,
    QWidget)

from sknrf.app.dataviewer.view.datagroup.widgets import DatagroupTreeView

class Ui_datagroupFrame(object):
    def setupUi(self, datagroupFrame):
        if not datagroupFrame.objectName():
            datagroupFrame.setObjectName(u"datagroupFrame")
        datagroupFrame.resize(398, 790)
        datagroupFrame.setFrameShape(QFrame.StyledPanel)
        datagroupFrame.setFrameShadow(QFrame.Raised)
        self.gridLayout = QGridLayout(datagroupFrame)
        self.gridLayout.setObjectName(u"gridLayout")
        self.importLineEdit = QLineEdit(datagroupFrame)
        self.importLineEdit.setObjectName(u"importLineEdit")
        self.importLineEdit.setEnabled(True)

        self.gridLayout.addWidget(self.importLineEdit, 1, 0, 1, 1)

        self.importBrowseButton = QPushButton(datagroupFrame)
        self.importBrowseButton.setObjectName(u"importBrowseButton")
        sizePolicy = QSizePolicy(QSizePolicy.Fixed, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.importBrowseButton.sizePolicy().hasHeightForWidth())
        self.importBrowseButton.setSizePolicy(sizePolicy)

        self.gridLayout.addWidget(self.importBrowseButton, 1, 1, 1, 1)

        self.datagroupLineEdit = QLineEdit(datagroupFrame)
        self.datagroupLineEdit.setObjectName(u"datagroupLineEdit")

        self.gridLayout.addWidget(self.datagroupLineEdit, 2, 0, 1, 1)

        self.datagroupBrowseButton = QPushButton(datagroupFrame)
        self.datagroupBrowseButton.setObjectName(u"datagroupBrowseButton")
        sizePolicy.setHeightForWidth(self.datagroupBrowseButton.sizePolicy().hasHeightForWidth())
        self.datagroupBrowseButton.setSizePolicy(sizePolicy)

        self.gridLayout.addWidget(self.datagroupBrowseButton, 2, 1, 1, 1)

        self.datagroupTreeView = DatagroupTreeView(datagroupFrame)
        self.datagroupTreeView.setObjectName(u"datagroupTreeView")
        self.datagroupTreeView.setSelectionMode(QAbstractItemView.ExtendedSelection)

        self.gridLayout.addWidget(self.datagroupTreeView, 3, 0, 1, 2)

        self.diffGroupBox = QGroupBox(datagroupFrame)
        self.diffGroupBox.setObjectName(u"diffGroupBox")
        self.diffGroupBox.setEnabled(True)
        self.horizontalLayout = QHBoxLayout(self.diffGroupBox)
        self.horizontalLayout.setSpacing(0)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.absPushButton = QPushButton(self.diffGroupBox)
        self.absPushButton.setObjectName(u"absPushButton")
        self.absPushButton.setEnabled(False)

        self.horizontalLayout.addWidget(self.absPushButton)

        self.relPushButton = QPushButton(self.diffGroupBox)
        self.relPushButton.setObjectName(u"relPushButton")
        self.relPushButton.setEnabled(False)

        self.horizontalLayout.addWidget(self.relPushButton)

        self.ratioPushButton = QPushButton(self.diffGroupBox)
        self.ratioPushButton.setObjectName(u"ratioPushButton")
        self.ratioPushButton.setEnabled(False)

        self.horizontalLayout.addWidget(self.ratioPushButton)

        self.customPushButton = QPushButton(self.diffGroupBox)
        self.customPushButton.setObjectName(u"customPushButton")
        self.customPushButton.setEnabled(False)

        self.horizontalLayout.addWidget(self.customPushButton)


        self.gridLayout.addWidget(self.diffGroupBox, 4, 0, 1, 2)

        self.line_2 = QFrame(datagroupFrame)
        self.line_2.setObjectName(u"line_2")
        self.line_2.setFrameShape(QFrame.HLine)
        self.line_2.setFrameShadow(QFrame.Sunken)

        self.gridLayout.addWidget(self.line_2, 5, 0, 1, 2)

        self.toolbarFrame = QFrame(datagroupFrame)
        self.toolbarFrame.setObjectName(u"toolbarFrame")
        sizePolicy1 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.toolbarFrame.sizePolicy().hasHeightForWidth())
        self.toolbarFrame.setSizePolicy(sizePolicy1)
        self.toolbarFrame.setFrameShape(QFrame.WinPanel)
        self.toolbarFrame.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_2 = QHBoxLayout(self.toolbarFrame)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalLayout_2.setContentsMargins(0, 0, 0, 0)
        self.refreshToolButton = QToolButton(self.toolbarFrame)
        self.refreshToolButton.setObjectName(u"refreshToolButton")
        icon = QIcon(QIcon.fromTheme(u"view-refresh"))
        self.refreshToolButton.setIcon(icon)
        self.refreshToolButton.setIconSize(QSize(24, 24))
        self.refreshToolButton.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.refreshToolButton.setAutoRaise(True)

        self.horizontalLayout_2.addWidget(self.refreshToolButton)

        self.line = QFrame(self.toolbarFrame)
        self.line.setObjectName(u"line")
        self.line.setFrameShape(QFrame.VLine)
        self.line.setFrameShadow(QFrame.Sunken)

        self.horizontalLayout_2.addWidget(self.line)

        self.clearToolButton = QToolButton(self.toolbarFrame)
        self.clearToolButton.setObjectName(u"clearToolButton")
        icon1 = QIcon(QIcon.fromTheme(u"edit-clear"))
        self.clearToolButton.setIcon(icon1)
        self.clearToolButton.setIconSize(QSize(24, 24))
        self.clearToolButton.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.clearToolButton.setAutoRaise(True)

        self.horizontalLayout_2.addWidget(self.clearToolButton)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer)


        self.gridLayout.addWidget(self.toolbarFrame, 0, 0, 1, 2)

        QWidget.setTabOrder(self.refreshToolButton, self.clearToolButton)
        QWidget.setTabOrder(self.clearToolButton, self.importLineEdit)
        QWidget.setTabOrder(self.importLineEdit, self.importBrowseButton)
        QWidget.setTabOrder(self.importBrowseButton, self.datagroupLineEdit)
        QWidget.setTabOrder(self.datagroupLineEdit, self.datagroupBrowseButton)
        QWidget.setTabOrder(self.datagroupBrowseButton, self.datagroupTreeView)
        QWidget.setTabOrder(self.datagroupTreeView, self.absPushButton)
        QWidget.setTabOrder(self.absPushButton, self.relPushButton)
        QWidget.setTabOrder(self.relPushButton, self.ratioPushButton)
        QWidget.setTabOrder(self.ratioPushButton, self.customPushButton)

        self.retranslateUi(datagroupFrame)

        QMetaObject.connectSlotsByName(datagroupFrame)
    # setupUi

    def retranslateUi(self, datagroupFrame):
        datagroupFrame.setWindowTitle(QCoreApplication.translate("datagroupFrame", u"Frame", None))
#if QT_CONFIG(whatsthis)
        self.importLineEdit.setWhatsThis(QCoreApplication.translate("datagroupFrame", u"<html><head/><body><p><br/></p></body></html>", None))
#endif // QT_CONFIG(whatsthis)
        self.importLineEdit.setText("")
        self.importLineEdit.setPlaceholderText(QCoreApplication.translate("datagroupFrame", u"Import Module Name", None))
        self.importBrowseButton.setText(QCoreApplication.translate("datagroupFrame", u"Browse", None))
        self.datagroupLineEdit.setPlaceholderText(QCoreApplication.translate("datagroupFrame", u"Dataset Filename", None))
        self.datagroupBrowseButton.setText(QCoreApplication.translate("datagroupFrame", u"Browse", None))
        self.diffGroupBox.setTitle(QCoreApplication.translate("datagroupFrame", u"Diff:", None))
        self.absPushButton.setText(QCoreApplication.translate("datagroupFrame", u"Abs", None))
        self.relPushButton.setText(QCoreApplication.translate("datagroupFrame", u"Rel", None))
        self.ratioPushButton.setText(QCoreApplication.translate("datagroupFrame", u"Ratio", None))
        self.customPushButton.setText(QCoreApplication.translate("datagroupFrame", u"Custom", None))
#if QT_CONFIG(tooltip)
        self.refreshToolButton.setToolTip(QCoreApplication.translate("datagroupFrame", u"Refresh", None))
#endif // QT_CONFIG(tooltip)
        self.refreshToolButton.setText(QCoreApplication.translate("datagroupFrame", u"Refresh", None))
#if QT_CONFIG(tooltip)
        self.clearToolButton.setToolTip(QCoreApplication.translate("datagroupFrame", u"Clear", None))
#endif // QT_CONFIG(tooltip)
        self.clearToolButton.setText(QCoreApplication.translate("datagroupFrame", u"Clear", None))
    # retranslateUi

