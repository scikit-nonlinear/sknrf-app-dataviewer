# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'QEquationFrame.ui'
##
## Created by: Qt User Interface Compiler version 6.4.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QAbstractItemView, QApplication, QCheckBox, QComboBox,
    QCommandLinkButton, QFrame, QGridLayout, QHBoxLayout,
    QHeaderView, QLabel, QLineEdit, QSizePolicy,
    QSpacerItem, QSpinBox, QToolButton, QWidget)

from sknrf.app.dataviewer.view.equation.widget import EquationTableView

class Ui_equationFrame(object):
    def setupUi(self, equationFrame):
        if not equationFrame.objectName():
            equationFrame.setObjectName(u"equationFrame")
        equationFrame.resize(496, 798)
        equationFrame.setFrameShape(QFrame.StyledPanel)
        equationFrame.setFrameShadow(QFrame.Raised)
        self.gridLayout = QGridLayout(equationFrame)
        self.gridLayout.setObjectName(u"gridLayout")
        self.selectAxesLabel = QLabel(equationFrame)
        self.selectAxesLabel.setObjectName(u"selectAxesLabel")

        self.gridLayout.addWidget(self.selectAxesLabel, 1, 0, 1, 1)

        self.axesTitleLineEdit = QLineEdit(equationFrame)
        self.axesTitleLineEdit.setObjectName(u"axesTitleLineEdit")

        self.gridLayout.addWidget(self.axesTitleLineEdit, 3, 1, 1, 7)

        self.titleLabel = QLabel(equationFrame)
        self.titleLabel.setObjectName(u"titleLabel")

        self.gridLayout.addWidget(self.titleLabel, 3, 0, 1, 1)

        self.selectedAxesComboBox = QComboBox(equationFrame)
        self.selectedAxesComboBox.addItem("")
        self.selectedAxesComboBox.setObjectName(u"selectedAxesComboBox")

        self.gridLayout.addWidget(self.selectedAxesComboBox, 1, 1, 1, 7)

        self.axesTitleLabel = QLabel(equationFrame)
        self.axesTitleLabel.setObjectName(u"axesTitleLabel")

        self.gridLayout.addWidget(self.axesTitleLabel, 2, 0, 1, 1)

        self.axesComboBox = QComboBox(equationFrame)
        self.axesComboBox.addItem("")
        self.axesComboBox.addItem("")
        self.axesComboBox.addItem("")
        self.axesComboBox.setObjectName(u"axesComboBox")

        self.gridLayout.addWidget(self.axesComboBox, 2, 1, 1, 7)

        self.majorGridCheckBox = QCheckBox(equationFrame)
        self.majorGridCheckBox.setObjectName(u"majorGridCheckBox")
        self.majorGridCheckBox.setChecked(True)

        self.gridLayout.addWidget(self.majorGridCheckBox, 4, 1, 1, 1)

        self.minorGridCheckBox = QCheckBox(equationFrame)
        self.minorGridCheckBox.setObjectName(u"minorGridCheckBox")
        self.minorGridCheckBox.setChecked(True)

        self.gridLayout.addWidget(self.minorGridCheckBox, 4, 4, 1, 2)

        self.gridLabel = QLabel(equationFrame)
        self.gridLabel.setObjectName(u"gridLabel")

        self.gridLayout.addWidget(self.gridLabel, 4, 0, 1, 1)

        self.selectedPlotComboBox = QComboBox(equationFrame)
        self.selectedPlotComboBox.setObjectName(u"selectedPlotComboBox")

        self.gridLayout.addWidget(self.selectedPlotComboBox, 11, 1, 1, 7)

        self.selectedPlotLabel = QLabel(equationFrame)
        self.selectedPlotLabel.setObjectName(u"selectedPlotLabel")

        self.gridLayout.addWidget(self.selectedPlotLabel, 11, 0, 1, 1)

        self.plotLabel = QLabel(equationFrame)
        self.plotLabel.setObjectName(u"plotLabel")

        self.gridLayout.addWidget(self.plotLabel, 12, 0, 1, 1)

        self.xStrLabel = QLabel(equationFrame)
        self.xStrLabel.setObjectName(u"xStrLabel")

        self.gridLayout.addWidget(self.xStrLabel, 13, 0, 1, 1)

        self.xStrLineEdit = QLineEdit(equationFrame)
        self.xStrLineEdit.setObjectName(u"xStrLineEdit")

        self.gridLayout.addWidget(self.xStrLineEdit, 13, 1, 1, 6)

        self.yTableView = EquationTableView(equationFrame)
        self.yTableView.setObjectName(u"yTableView")
        self.yTableView.setStyleSheet(u"QTableView { selection-background-color: palette(Highlight); }")
        self.yTableView.setAlternatingRowColors(True)
        self.yTableView.setSelectionMode(QAbstractItemView.SingleSelection)
        self.yTableView.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.yTableView.horizontalHeader().setStretchLastSection(True)
        self.yTableView.verticalHeader().setVisible(False)

        self.gridLayout.addWidget(self.yTableView, 10, 3, 1, 5)

        self.plotComboBox = QComboBox(equationFrame)
        self.plotComboBox.setObjectName(u"plotComboBox")

        self.gridLayout.addWidget(self.plotComboBox, 12, 1, 1, 7)

        self.xFormatComboBox = QComboBox(equationFrame)
        self.xFormatComboBox.addItem("")
        self.xFormatComboBox.addItem("")
        self.xFormatComboBox.addItem("")
        self.xFormatComboBox.addItem("")
        self.xFormatComboBox.addItem("")
        self.xFormatComboBox.addItem("")
        self.xFormatComboBox.addItem("")
        self.xFormatComboBox.setObjectName(u"xFormatComboBox")

        self.gridLayout.addWidget(self.xFormatComboBox, 13, 7, 1, 1)

        self.yFormatComboBox = QComboBox(equationFrame)
        self.yFormatComboBox.addItem("")
        self.yFormatComboBox.addItem("")
        self.yFormatComboBox.addItem("")
        self.yFormatComboBox.addItem("")
        self.yFormatComboBox.addItem("")
        self.yFormatComboBox.addItem("")
        self.yFormatComboBox.addItem("")
        self.yFormatComboBox.setObjectName(u"yFormatComboBox")

        self.gridLayout.addWidget(self.yFormatComboBox, 14, 7, 1, 1)

        self.yStrLabel = QLabel(equationFrame)
        self.yStrLabel.setObjectName(u"yStrLabel")

        self.gridLayout.addWidget(self.yStrLabel, 14, 0, 1, 1)

        self.yStrLineEdit = QLineEdit(equationFrame)
        self.yStrLineEdit.setObjectName(u"yStrLineEdit")

        self.gridLayout.addWidget(self.yStrLineEdit, 14, 1, 1, 6)

        self.lineStyleLabel = QLabel(equationFrame)
        self.lineStyleLabel.setObjectName(u"lineStyleLabel")

        self.gridLayout.addWidget(self.lineStyleLabel, 15, 1, 1, 1)

        self.lineCheckBox = QCheckBox(equationFrame)
        self.lineCheckBox.setObjectName(u"lineCheckBox")

        self.gridLayout.addWidget(self.lineCheckBox, 15, 0, 1, 1)

        self.lineSizeSpinBox = QSpinBox(equationFrame)
        self.lineSizeSpinBox.setObjectName(u"lineSizeSpinBox")
        self.lineSizeSpinBox.setMinimum(1)
        self.lineSizeSpinBox.setValue(2)

        self.gridLayout.addWidget(self.lineSizeSpinBox, 15, 6, 1, 2)

        self.lineSizeLabel = QLabel(equationFrame)
        self.lineSizeLabel.setObjectName(u"lineSizeLabel")

        self.gridLayout.addWidget(self.lineSizeLabel, 15, 5, 1, 1)

        self.markerCheckBox = QCheckBox(equationFrame)
        self.markerCheckBox.setObjectName(u"markerCheckBox")
        self.markerCheckBox.setChecked(True)

        self.gridLayout.addWidget(self.markerCheckBox, 16, 0, 1, 1)

        self.markerSizeLabel = QLabel(equationFrame)
        self.markerSizeLabel.setObjectName(u"markerSizeLabel")

        self.gridLayout.addWidget(self.markerSizeLabel, 16, 5, 1, 1)

        self.markerStyleLabel = QLabel(equationFrame)
        self.markerStyleLabel.setObjectName(u"markerStyleLabel")

        self.gridLayout.addWidget(self.markerStyleLabel, 16, 1, 1, 1)

        self.optionsLineEdit = QLineEdit(equationFrame)
        self.optionsLineEdit.setObjectName(u"optionsLineEdit")

        self.gridLayout.addWidget(self.optionsLineEdit, 17, 1, 1, 7)

        self.optionsLabel = QLabel(equationFrame)
        self.optionsLabel.setObjectName(u"optionsLabel")

        self.gridLayout.addWidget(self.optionsLabel, 17, 0, 1, 1)

        self.line_4 = QFrame(equationFrame)
        self.line_4.setObjectName(u"line_4")
        self.line_4.setFrameShape(QFrame.HLine)
        self.line_4.setFrameShadow(QFrame.Sunken)

        self.gridLayout.addWidget(self.line_4, 18, 0, 1, 8)

        self.plotPushButton = QCommandLinkButton(equationFrame)
        self.plotPushButton.setObjectName(u"plotPushButton")

        self.gridLayout.addWidget(self.plotPushButton, 19, 0, 1, 8)

        self.transformLabel = QLabel(equationFrame)
        self.transformLabel.setObjectName(u"transformLabel")

        self.gridLayout.addWidget(self.transformLabel, 8, 0, 1, 1)

        self.line = QFrame(equationFrame)
        self.line.setObjectName(u"line")
        self.line.setFrameShape(QFrame.HLine)
        self.line.setFrameShadow(QFrame.Sunken)

        self.gridLayout.addWidget(self.line, 7, 0, 1, 8)

        self.transformComboBox = QComboBox(equationFrame)
        self.transformComboBox.setObjectName(u"transformComboBox")

        self.gridLayout.addWidget(self.transformComboBox, 8, 1, 1, 7)

        self.xEquationLabel = QLabel(equationFrame)
        self.xEquationLabel.setObjectName(u"xEquationLabel")

        self.gridLayout.addWidget(self.xEquationLabel, 9, 0, 1, 3)

        self.xEquationLabel_2 = QLabel(equationFrame)
        self.xEquationLabel_2.setObjectName(u"xEquationLabel_2")

        self.gridLayout.addWidget(self.xEquationLabel_2, 9, 3, 1, 5)

        self.xTableView = EquationTableView(equationFrame)
        self.xTableView.setObjectName(u"xTableView")
        self.xTableView.setStyleSheet(u"QTableView { selection-background-color: palette(Highlight); }")
        self.xTableView.setAlternatingRowColors(True)
        self.xTableView.setSelectionMode(QAbstractItemView.SingleSelection)
        self.xTableView.setSelectionBehavior(QAbstractItemView.SelectRows)
        self.xTableView.horizontalHeader().setStretchLastSection(True)
        self.xTableView.verticalHeader().setVisible(False)
        self.xTableView.verticalHeader().setHighlightSections(True)

        self.gridLayout.addWidget(self.xTableView, 10, 0, 1, 3)

        self.lineStyleComboBox = QComboBox(equationFrame)
        self.lineStyleComboBox.setObjectName(u"lineStyleComboBox")

        self.gridLayout.addWidget(self.lineStyleComboBox, 15, 2, 1, 3)

        self.markerSizeSpinBox = QSpinBox(equationFrame)
        self.markerSizeSpinBox.setObjectName(u"markerSizeSpinBox")
        self.markerSizeSpinBox.setMinimum(1)
        self.markerSizeSpinBox.setValue(2)

        self.gridLayout.addWidget(self.markerSizeSpinBox, 16, 6, 1, 2)

        self.xAxisComboBox = QComboBox(equationFrame)
        self.xAxisComboBox.addItem("")
        self.xAxisComboBox.addItem("")
        self.xAxisComboBox.setObjectName(u"xAxisComboBox")

        self.gridLayout.addWidget(self.xAxisComboBox, 5, 7, 1, 1)

        self.xOriginLabel = QLabel(equationFrame)
        self.xOriginLabel.setObjectName(u"xOriginLabel")
        self.xOriginLabel.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.xOriginLabel, 5, 1, 1, 1)

        self.xOriginSpinBox = QSpinBox(equationFrame)
        self.xOriginSpinBox.setObjectName(u"xOriginSpinBox")
        self.xOriginSpinBox.setMaximum(10)

        self.gridLayout.addWidget(self.xOriginSpinBox, 5, 2, 1, 1)

        self.xSpanLabel = QLabel(equationFrame)
        self.xSpanLabel.setObjectName(u"xSpanLabel")
        self.xSpanLabel.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.xSpanLabel, 5, 4, 1, 1)

        self.yAxisComboBox = QComboBox(equationFrame)
        self.yAxisComboBox.addItem("")
        self.yAxisComboBox.addItem("")
        self.yAxisComboBox.setObjectName(u"yAxisComboBox")

        self.gridLayout.addWidget(self.yAxisComboBox, 6, 7, 1, 1)

        self.ySpanLabel = QLabel(equationFrame)
        self.ySpanLabel.setObjectName(u"ySpanLabel")
        self.ySpanLabel.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.ySpanLabel, 6, 4, 1, 1)

        self.yOriginSpinBox = QSpinBox(equationFrame)
        self.yOriginSpinBox.setObjectName(u"yOriginSpinBox")
        self.yOriginSpinBox.setMaximum(10)

        self.gridLayout.addWidget(self.yOriginSpinBox, 6, 2, 1, 1)

        self.yOriginLabel = QLabel(equationFrame)
        self.yOriginLabel.setObjectName(u"yOriginLabel")
        self.yOriginLabel.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.yOriginLabel, 6, 1, 1, 1)

        self.ySpanSpinBox = QSpinBox(equationFrame)
        self.ySpanSpinBox.setObjectName(u"ySpanSpinBox")
        self.ySpanSpinBox.setMinimum(1)
        self.ySpanSpinBox.setMaximum(10)
        self.ySpanSpinBox.setValue(1)

        self.gridLayout.addWidget(self.ySpanSpinBox, 6, 5, 1, 1)

        self.yAxisLabel = QLabel(equationFrame)
        self.yAxisLabel.setObjectName(u"yAxisLabel")
        self.yAxisLabel.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.yAxisLabel, 6, 6, 1, 1)

        self.xSpanSpinBox = QSpinBox(equationFrame)
        self.xSpanSpinBox.setObjectName(u"xSpanSpinBox")
        self.xSpanSpinBox.setMinimum(1)
        self.xSpanSpinBox.setMaximum(10)

        self.gridLayout.addWidget(self.xSpanSpinBox, 5, 5, 1, 1)

        self.markerStyleComboBox = QComboBox(equationFrame)
        self.markerStyleComboBox.setObjectName(u"markerStyleComboBox")

        self.gridLayout.addWidget(self.markerStyleComboBox, 16, 2, 1, 3)

        self.toolbarFrame = QFrame(equationFrame)
        self.toolbarFrame.setObjectName(u"toolbarFrame")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.toolbarFrame.sizePolicy().hasHeightForWidth())
        self.toolbarFrame.setSizePolicy(sizePolicy)
        self.toolbarFrame.setFrameShape(QFrame.WinPanel)
        self.toolbarFrame.setFrameShadow(QFrame.Raised)
        self.horizontalLayout = QHBoxLayout(self.toolbarFrame)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.panToolButton = QToolButton(self.toolbarFrame)
        self.panToolButton.setObjectName(u"panToolButton")
        icon = QIcon(QIcon.fromTheme(u"view-restore"))
        self.panToolButton.setIcon(icon)
        self.panToolButton.setIconSize(QSize(24, 24))
        self.panToolButton.setCheckable(True)
        self.panToolButton.setChecked(False)
        self.panToolButton.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.panToolButton.setAutoRaise(True)

        self.horizontalLayout.addWidget(self.panToolButton)

        self.zoomToolButton = QToolButton(self.toolbarFrame)
        self.zoomToolButton.setObjectName(u"zoomToolButton")
        icon1 = QIcon(QIcon.fromTheme(u"zoom-in"))
        self.zoomToolButton.setIcon(icon1)
        self.zoomToolButton.setIconSize(QSize(24, 24))
        self.zoomToolButton.setCheckable(True)
        self.zoomToolButton.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.zoomToolButton.setAutoRaise(True)

        self.horizontalLayout.addWidget(self.zoomToolButton)

        self.homeToolButton = QToolButton(self.toolbarFrame)
        self.homeToolButton.setObjectName(u"homeToolButton")
        icon2 = QIcon(QIcon.fromTheme(u"zoom-original"))
        self.homeToolButton.setIcon(icon2)
        self.homeToolButton.setIconSize(QSize(24, 24))
        self.homeToolButton.setCheckable(False)
        self.homeToolButton.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.homeToolButton.setAutoRaise(True)

        self.horizontalLayout.addWidget(self.homeToolButton)

        self.settingsToolButton = QToolButton(self.toolbarFrame)
        self.settingsToolButton.setObjectName(u"settingsToolButton")
        icon3 = QIcon(QIcon.fromTheme(u"applications-system"))
        self.settingsToolButton.setIcon(icon3)
        self.settingsToolButton.setIconSize(QSize(24, 24))
        self.settingsToolButton.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.settingsToolButton.setAutoRaise(True)

        self.horizontalLayout.addWidget(self.settingsToolButton)

        self.line_2 = QFrame(self.toolbarFrame)
        self.line_2.setObjectName(u"line_2")
        self.line_2.setFrameShape(QFrame.VLine)
        self.line_2.setFrameShadow(QFrame.Sunken)

        self.horizontalLayout.addWidget(self.line_2)

        self.clearToolButton = QToolButton(self.toolbarFrame)
        self.clearToolButton.setObjectName(u"clearToolButton")
        icon4 = QIcon(QIcon.fromTheme(u"edit-clear"))
        self.clearToolButton.setIcon(icon4)
        self.clearToolButton.setIconSize(QSize(24, 24))
        self.clearToolButton.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.clearToolButton.setAutoRaise(True)

        self.horizontalLayout.addWidget(self.clearToolButton)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)


        self.gridLayout.addWidget(self.toolbarFrame, 0, 0, 1, 8)

        self.xAxisLabel = QLabel(equationFrame)
        self.xAxisLabel.setObjectName(u"xAxisLabel")
        self.xAxisLabel.setAlignment(Qt.AlignRight|Qt.AlignTrailing|Qt.AlignVCenter)

        self.gridLayout.addWidget(self.xAxisLabel, 5, 6, 1, 1)

        self.yShareAxisCheckBox = QCheckBox(equationFrame)
        self.yShareAxisCheckBox.setObjectName(u"yShareAxisCheckBox")

        self.gridLayout.addWidget(self.yShareAxisCheckBox, 6, 0, 1, 1)

        self.xShareAxisCheckBox = QCheckBox(equationFrame)
        self.xShareAxisCheckBox.setObjectName(u"xShareAxisCheckBox")

        self.gridLayout.addWidget(self.xShareAxisCheckBox, 5, 0, 1, 1)

#if QT_CONFIG(shortcut)
        self.selectAxesLabel.setBuddy(self.selectedAxesComboBox)
        self.titleLabel.setBuddy(self.axesTitleLineEdit)
        self.axesTitleLabel.setBuddy(self.axesComboBox)
        self.gridLabel.setBuddy(self.majorGridCheckBox)
        self.xStrLabel.setBuddy(self.xStrLineEdit)
        self.yStrLabel.setBuddy(self.yStrLineEdit)
        self.optionsLabel.setBuddy(self.optionsLineEdit)
        self.transformLabel.setBuddy(self.transformComboBox)
        self.xEquationLabel.setBuddy(self.xStrLineEdit)
        self.xEquationLabel_2.setBuddy(self.yStrLineEdit)
        self.xOriginLabel.setBuddy(self.xOriginSpinBox)
        self.xSpanLabel.setBuddy(self.xSpanSpinBox)
        self.ySpanLabel.setBuddy(self.ySpanSpinBox)
        self.yOriginLabel.setBuddy(self.yOriginSpinBox)
        self.yAxisLabel.setBuddy(self.yAxisComboBox)
        self.xAxisLabel.setBuddy(self.xAxisComboBox)
#endif // QT_CONFIG(shortcut)
        QWidget.setTabOrder(self.panToolButton, self.zoomToolButton)
        QWidget.setTabOrder(self.zoomToolButton, self.homeToolButton)
        QWidget.setTabOrder(self.homeToolButton, self.settingsToolButton)
        QWidget.setTabOrder(self.settingsToolButton, self.clearToolButton)
        QWidget.setTabOrder(self.clearToolButton, self.selectedAxesComboBox)
        QWidget.setTabOrder(self.selectedAxesComboBox, self.axesComboBox)
        QWidget.setTabOrder(self.axesComboBox, self.axesTitleLineEdit)
        QWidget.setTabOrder(self.axesTitleLineEdit, self.xShareAxisCheckBox)
        QWidget.setTabOrder(self.xShareAxisCheckBox, self.xOriginSpinBox)
        QWidget.setTabOrder(self.xOriginSpinBox, self.xSpanSpinBox)
        QWidget.setTabOrder(self.xSpanSpinBox, self.xAxisComboBox)
        QWidget.setTabOrder(self.xAxisComboBox, self.yShareAxisCheckBox)
        QWidget.setTabOrder(self.yShareAxisCheckBox, self.yOriginSpinBox)
        QWidget.setTabOrder(self.yOriginSpinBox, self.ySpanSpinBox)
        QWidget.setTabOrder(self.ySpanSpinBox, self.yAxisComboBox)
        QWidget.setTabOrder(self.yAxisComboBox, self.transformComboBox)
        QWidget.setTabOrder(self.transformComboBox, self.xTableView)
        QWidget.setTabOrder(self.xTableView, self.yTableView)
        QWidget.setTabOrder(self.yTableView, self.xStrLineEdit)
        QWidget.setTabOrder(self.xStrLineEdit, self.xFormatComboBox)
        QWidget.setTabOrder(self.xFormatComboBox, self.yStrLineEdit)
        QWidget.setTabOrder(self.yStrLineEdit, self.yFormatComboBox)
        QWidget.setTabOrder(self.yFormatComboBox, self.majorGridCheckBox)
        QWidget.setTabOrder(self.majorGridCheckBox, self.minorGridCheckBox)
        QWidget.setTabOrder(self.minorGridCheckBox, self.lineCheckBox)
        QWidget.setTabOrder(self.lineCheckBox, self.lineStyleComboBox)
        QWidget.setTabOrder(self.lineStyleComboBox, self.markerCheckBox)
        QWidget.setTabOrder(self.markerCheckBox, self.markerSizeSpinBox)
        QWidget.setTabOrder(self.markerSizeSpinBox, self.markerStyleComboBox)
        QWidget.setTabOrder(self.markerStyleComboBox, self.optionsLineEdit)
        QWidget.setTabOrder(self.optionsLineEdit, self.plotPushButton)

        self.retranslateUi(equationFrame)

        QMetaObject.connectSlotsByName(equationFrame)
    # setupUi

    def retranslateUi(self, equationFrame):
        equationFrame.setWindowTitle(QCoreApplication.translate("equationFrame", u"Frame", None))
        self.selectAxesLabel.setText(QCoreApplication.translate("equationFrame", u"Select Axes:", None))
        self.titleLabel.setText(QCoreApplication.translate("equationFrame", u"Axes Title:", None))
        self.selectedAxesComboBox.setItemText(0, QCoreApplication.translate("equationFrame", u"< New Axes >", None))

        self.axesTitleLabel.setText(QCoreApplication.translate("equationFrame", u"Axes Type:", None))
        self.axesComboBox.setItemText(0, QCoreApplication.translate("equationFrame", u"Rectangular", None))
        self.axesComboBox.setItemText(1, QCoreApplication.translate("equationFrame", u"Polar", None))
        self.axesComboBox.setItemText(2, QCoreApplication.translate("equationFrame", u"Smith", None))

        self.majorGridCheckBox.setText(QCoreApplication.translate("equationFrame", u"Major", None))
        self.minorGridCheckBox.setText(QCoreApplication.translate("equationFrame", u"Minor", None))
        self.gridLabel.setText(QCoreApplication.translate("equationFrame", u"Axes Grid:", None))
        self.selectedPlotLabel.setText(QCoreApplication.translate("equationFrame", u"Select Plot:", None))
        self.plotLabel.setText(QCoreApplication.translate("equationFrame", u"Plot Type:", None))
        self.xStrLabel.setText(QCoreApplication.translate("equationFrame", u"X Label", None))
        self.xFormatComboBox.setItemText(0, QCoreApplication.translate("equationFrame", u"Lin Mag", None))
        self.xFormatComboBox.setItemText(1, QCoreApplication.translate("equationFrame", u"dBm", None))
        self.xFormatComboBox.setItemText(2, QCoreApplication.translate("equationFrame", u"dB", None))
        self.xFormatComboBox.setItemText(3, QCoreApplication.translate("equationFrame", u"Re", None))
        self.xFormatComboBox.setItemText(4, QCoreApplication.translate("equationFrame", u"Im", None))
        self.xFormatComboBox.setItemText(5, QCoreApplication.translate("equationFrame", u"Angle Deg", None))
        self.xFormatComboBox.setItemText(6, QCoreApplication.translate("equationFrame", u"Angle Rad", None))

        self.yFormatComboBox.setItemText(0, QCoreApplication.translate("equationFrame", u"Lin Mag", None))
        self.yFormatComboBox.setItemText(1, QCoreApplication.translate("equationFrame", u"dBm", None))
        self.yFormatComboBox.setItemText(2, QCoreApplication.translate("equationFrame", u"dB", None))
        self.yFormatComboBox.setItemText(3, QCoreApplication.translate("equationFrame", u"Re", None))
        self.yFormatComboBox.setItemText(4, QCoreApplication.translate("equationFrame", u"Im", None))
        self.yFormatComboBox.setItemText(5, QCoreApplication.translate("equationFrame", u"Angle Deg", None))
        self.yFormatComboBox.setItemText(6, QCoreApplication.translate("equationFrame", u"Angle Rad", None))

        self.yStrLabel.setText(QCoreApplication.translate("equationFrame", u"Y Label:", None))
        self.lineStyleLabel.setText(QCoreApplication.translate("equationFrame", u"Style:", None))
        self.lineCheckBox.setText(QCoreApplication.translate("equationFrame", u"Line:", None))
        self.lineSizeLabel.setText(QCoreApplication.translate("equationFrame", u"Size:", None))
        self.markerCheckBox.setText(QCoreApplication.translate("equationFrame", u"Marker:", None))
        self.markerSizeLabel.setText(QCoreApplication.translate("equationFrame", u"Size:", None))
        self.markerStyleLabel.setText(QCoreApplication.translate("equationFrame", u"Style:", None))
        self.optionsLabel.setText(QCoreApplication.translate("equationFrame", u"Options:", None))
        self.plotPushButton.setText(QCoreApplication.translate("equationFrame", u"Create New Plot", None))
#if QT_CONFIG(shortcut)
        self.plotPushButton.setShortcut(QCoreApplication.translate("equationFrame", u"Ctrl+Return", None))
#endif // QT_CONFIG(shortcut)
        self.transformLabel.setText(QCoreApplication.translate("equationFrame", u"Transform:", None))
        self.xEquationLabel.setText(QCoreApplication.translate("equationFrame", u"X:", None))
        self.xEquationLabel_2.setText(QCoreApplication.translate("equationFrame", u"Y:", None))
        self.xAxisComboBox.setItemText(0, QCoreApplication.translate("equationFrame", u"linear", None))
        self.xAxisComboBox.setItemText(1, QCoreApplication.translate("equationFrame", u"log", None))

        self.xOriginLabel.setText(QCoreApplication.translate("equationFrame", u"Origin:", None))
        self.xSpanLabel.setText(QCoreApplication.translate("equationFrame", u"Span:", None))
        self.yAxisComboBox.setItemText(0, QCoreApplication.translate("equationFrame", u"linear", None))
        self.yAxisComboBox.setItemText(1, QCoreApplication.translate("equationFrame", u"log", None))

        self.ySpanLabel.setText(QCoreApplication.translate("equationFrame", u"Span:", None))
        self.yOriginLabel.setText(QCoreApplication.translate("equationFrame", u"Origin:", None))
        self.yAxisLabel.setText(QCoreApplication.translate("equationFrame", u"Axis:", None))
#if QT_CONFIG(tooltip)
        self.panToolButton.setToolTip(QCoreApplication.translate("equationFrame", u"Pan left-mouse, Zoom right-mouse", None))
#endif // QT_CONFIG(tooltip)
        self.panToolButton.setText(QCoreApplication.translate("equationFrame", u"Pan", None))
#if QT_CONFIG(tooltip)
        self.zoomToolButton.setToolTip(QCoreApplication.translate("equationFrame", u"Zoom to Rectangle", None))
#endif // QT_CONFIG(tooltip)
        self.zoomToolButton.setText(QCoreApplication.translate("equationFrame", u"Zoom", None))
#if QT_CONFIG(tooltip)
        self.homeToolButton.setToolTip(QCoreApplication.translate("equationFrame", u"Auto-Scale", None))
#endif // QT_CONFIG(tooltip)
        self.homeToolButton.setText(QCoreApplication.translate("equationFrame", u"Home", None))
#if QT_CONFIG(tooltip)
        self.settingsToolButton.setToolTip(QCoreApplication.translate("equationFrame", u"Settings", None))
#endif // QT_CONFIG(tooltip)
        self.settingsToolButton.setText(QCoreApplication.translate("equationFrame", u"Settings", None))
#if QT_CONFIG(tooltip)
        self.clearToolButton.setToolTip(QCoreApplication.translate("equationFrame", u"Clear", None))
#endif // QT_CONFIG(tooltip)
        self.clearToolButton.setText(QCoreApplication.translate("equationFrame", u"Clear", None))
        self.xAxisLabel.setText(QCoreApplication.translate("equationFrame", u"Axis:", None))
        self.yShareAxisCheckBox.setText(QCoreApplication.translate("equationFrame", u"Share Y Axis:", None))
        self.xShareAxisCheckBox.setText(QCoreApplication.translate("equationFrame", u"Share X Axis:", None))
    # retranslateUi

