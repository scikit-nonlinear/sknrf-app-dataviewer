# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'QIndexFrame.ui'
##
## Created by: Qt User Interface Compiler version 6.4.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QFrame, QHBoxLayout, QSizePolicy,
    QSpacerItem, QToolBox, QToolButton, QVBoxLayout,
    QWidget)

class Ui_indexFrame(object):
    def setupUi(self, indexFrame):
        if not indexFrame.objectName():
            indexFrame.setObjectName(u"indexFrame")
        indexFrame.resize(389, 789)
        indexFrame.setFrameShape(QFrame.StyledPanel)
        indexFrame.setFrameShadow(QFrame.Raised)
        self.verticalLayout = QVBoxLayout(indexFrame)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.toolbarFrame = QFrame(indexFrame)
        self.toolbarFrame.setObjectName(u"toolbarFrame")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.toolbarFrame.sizePolicy().hasHeightForWidth())
        self.toolbarFrame.setSizePolicy(sizePolicy)
        self.toolbarFrame.setFrameShape(QFrame.WinPanel)
        self.toolbarFrame.setFrameShadow(QFrame.Raised)
        self.horizontalLayout_3 = QHBoxLayout(self.toolbarFrame)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.horizontalLayout_3.setContentsMargins(0, 0, 0, 0)
        self.markerToolButton = QToolButton(self.toolbarFrame)
        self.markerToolButton.setObjectName(u"markerToolButton")
        icon = QIcon(QIcon.fromTheme(u"media-record"))
        self.markerToolButton.setIcon(icon)
        self.markerToolButton.setIconSize(QSize(24, 24))
        self.markerToolButton.setCheckable(True)
        self.markerToolButton.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.markerToolButton.setAutoRaise(True)

        self.horizontalLayout_3.addWidget(self.markerToolButton)

        self.clearToolButton = QToolButton(self.toolbarFrame)
        self.clearToolButton.setObjectName(u"clearToolButton")
        icon1 = QIcon(QIcon.fromTheme(u"edit-clear"))
        self.clearToolButton.setIcon(icon1)
        self.clearToolButton.setIconSize(QSize(24, 24))
        self.clearToolButton.setToolButtonStyle(Qt.ToolButtonTextUnderIcon)
        self.clearToolButton.setAutoRaise(True)

        self.horizontalLayout_3.addWidget(self.clearToolButton)

        self.horizontalSpacer_2 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_3.addItem(self.horizontalSpacer_2)


        self.verticalLayout.addWidget(self.toolbarFrame)

        self.filtersToolbox = QToolBox(indexFrame)
        self.filtersToolbox.setObjectName(u"filtersToolbox")
        sizePolicy1 = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Expanding)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.filtersToolbox.sizePolicy().hasHeightForWidth())
        self.filtersToolbox.setSizePolicy(sizePolicy1)
        self.filter1 = QWidget()
        self.filter1.setObjectName(u"filter1")
        self.filter1.setGeometry(QRect(0, 0, 373, 318))
        self.verticalLayout_5 = QVBoxLayout(self.filter1)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.filtersToolbox.addItem(self.filter1, u"No Filters")

        self.verticalLayout.addWidget(self.filtersToolbox)

        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout.addItem(self.verticalSpacer)

        QWidget.setTabOrder(self.markerToolButton, self.clearToolButton)

        self.retranslateUi(indexFrame)

        self.filtersToolbox.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(indexFrame)
    # setupUi

    def retranslateUi(self, indexFrame):
        indexFrame.setWindowTitle(QCoreApplication.translate("indexFrame", u"Frame", None))
#if QT_CONFIG(tooltip)
        self.markerToolButton.setToolTip(QCoreApplication.translate("indexFrame", u"Undo", None))
#endif // QT_CONFIG(tooltip)
        self.markerToolButton.setText(QCoreApplication.translate("indexFrame", u"Marker", None))
#if QT_CONFIG(tooltip)
        self.clearToolButton.setToolTip(QCoreApplication.translate("indexFrame", u"Clear", None))
#endif // QT_CONFIG(tooltip)
        self.clearToolButton.setText(QCoreApplication.translate("indexFrame", u"Clear", None))
        self.filtersToolbox.setItemText(self.filtersToolbox.indexOf(self.filter1), QCoreApplication.translate("indexFrame", u"No Filters", None))
    # retranslateUi

