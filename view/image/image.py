import os
import logging

from PySide6 import QtCore
from PySide6.QtGui import QIcon
from PySide6.QtWidgets import QFrame
import matplotlib as mpl

from sknrf.app.dataviewer.view.QImageFrame_ui import Ui_imageFrame

logger = logging.getLogger()


class ImageFrame(QFrame, Ui_imageFrame):

    def __init__(self, parent=None):
        super(ImageFrame, self).__init__(parent=parent)
        self.setupUi(self)

    def connect_signals(self):
        self._parent.gctb().message.connect(self.set_coordinates)

    def disconnect_signals(self):
        self._parent.gctb().message.disconnect(self.set_coordinates)

    @QtCore.Slot(str)
    def set_coordinates(self, message):
        self.coordinatesLabel.setText(message)

    def update(self, plots=False):
        super(ImageFrame, self).update()
        self._parent.update(plots=plots)


